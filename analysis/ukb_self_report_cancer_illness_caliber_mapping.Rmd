---
title: "UKB self-reported cancer illnesses to CALIBER mapping"
author: "A Warwick and CY Ung"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: 
  workflowr::wflow_html:
    toc: true
    code_folding: hide
editor_options:
  chunk_output_type: console
---

```{r global-options, echo=FALSE}
knitr::opts_chunk$set(warning=FALSE, message=FALSE)
```

```{r}
library(tidyverse)
library(reactable)
library(readxl)
library(crosstalk)
library(targets)
library(codemapper)
library(flextable)

# read codes that map to opcs4
tar_load(read_codes_that_map_to_opcs4)

# caliber codes
tar_load(caliber_codes)

# self-reported UKB cancer illnesses
coding3 <- tar_read(ukb_codings) %>% 
  filter(Coding == "3")

# caliber codes - filter for diseases including opcs4 codes, then filter for OPCS4/read codes only, then filter read codes for only those that can map to OPCS4
caliber_codes_cancer <- caliber_codes %>% 
  # append disease categories and filter for 'Cancers' category only
  left_join(codemapper::get_caliber_categories_mapping() %>% 
              select(disease = phenotype,
                     caliber_category = category),
            by = "disease") %>% 
  filter(caliber_category == "Cancers")
```

# Overview

**Aim:** to assign UKB self-reported cancer illness codes to CALIBER conditions.

Approach:

-   Filter CALIBER codelists for diseases categorised as 'Cancer', then filter for ICD and Read codes only
-   Filter Read codes for only those in the read2/3-to-OPCS4 mapping tables
-   Manually review UKB self-reported operation codes and assign to CALIBER diseases (also manually review Read codes)
-   For the final code list, merge these with CALIBER codes excluding (i) OPCS4 codes and (ii) Read codes that map to an OPCS4 code.

## UKB self-reported cancer illnesses lookup table

```{r}
coding3 %>% 
  select(-Coding,
         data_coding_3 = Value,
         ukb_meaning = Meaning) %>% 
  reactable(filterable = TRUE,
            searchable = TRUE,
            paginationType = "jump",
            showPageSizeOptions = TRUE,
            resizable = TRUE,
            pageSizeOptions = c(10, 25, 50, 100, 200, 300, 350))
```

## CALIBER diseases categorised under 'Cancers'

```{r}
caliber_codes_cancer %>% 
  distinct(disease) %>% 
  reactable(filterable = TRUE,
            paginationType = "jump",
            showPageSizeOptions = TRUE,
            resizable = TRUE,
            pageSizeOptions = c(10, 30))
```

# Manual mapping of UKB self-reported operations codes to CALIBER diseases

## Write csv file for manual mapping

```{r}
# write to csv
ukb_self_report_cancer_to_caliber_map_unselected <-
  caliber_codes_cancer %>%
  distinct(disease) %>%
  mutate(
    description = "",
    category = "UKB self-reported",
    code_type = "data_coding_3",
    code = "",
    author = "caliber"
  )

ukb_self_report_cancer_to_caliber_map_unselected %>% 
  write_csv(here::here(file.path("data_small",
                                 "ukb_self_report_cancer_to_caliber_map_raw.csv")))

# NOTE: when manually reviewing, save the csv as a separate file without the '_raw' suffix
```

```{r results='hide'}
# test that `ukb_self_report_non_cancer_to_caliber_map.csv` contains the same mappings as `ukb_self_report_non_cancer_to_caliber_map_raw.csv` (apart from the manuall mapping column) - RAISE ERROR AND HALT KNITTING IF NOT
ukb_self_report_cancer_to_caliber_map <- read_csv(here::here(file.path("data_small",
                                                                           "ukb_self_report_cancer_to_caliber_map.csv")),
                                                      col_types = readr::cols(.default = "c"))

assertthat::are_equal(
  x = ukb_self_report_cancer_to_caliber_map %>% 
    distinct(disease) %>% 
    arrange(),
  y = ukb_self_report_cancer_to_caliber_map_unselected %>% 
    distinct(disease) %>% 
    arrange(),
  msg = "Manually selected mapping csv does not match 'ukb_self_report_cancer_to_caliber_map_raw.csv'. These should contain the same values under column 'disease'. Has the raw version been updated?"
)

# general validation checks
ukb_self_report_cancer_to_caliber_map %>% 
  filter(selected == "Y") %>% 
  select(-selected) %>% 
  ukbwranglr::validate_clinical_codes()

# check self-report codes/descriptions are correct
assertthat::are_equal(
  ukb_self_report_cancer_to_caliber_map %>%
    filter(selected == "Y") %>%
    select(-selected) %>%
    inner_join(coding3,
              by = c(
                "code" = "Value",
                "description" = "Meaning"
              )) %>%
    nrow(),
  nrow(
    ukb_self_report_cancer_to_caliber_map %>%
      filter(selected == "Y")
  )
)
```

## Review examples of questionable mappings

### 'Primary Malignancy_Other Skin and subcutaneous tissue'

The UKB self-reported codes for skin cancer include 'skin cancer', 'melanoma' and 'non-melanoma skin cancer'. The CALIBER codelist for 'Primary Malignancy_Other Skin and subcutaneous tissue' includes codes for BCC.

## Manually annotated mapping file

```{r}
ukb_self_report_cancer_to_caliber_map %>% 
  mutate(selected = case_when(is.na(selected) ~ "N",
                              TRUE ~ selected)) %>% 
  reactable(filterable = TRUE,
            searchable = TRUE,
            paginationType = "jump",
            showPageSizeOptions = TRUE,
            pageSizeOptions = c(10, 25, 50))
```

# Final codelist

CALIBER diseases categorised under 'Cancers', excluding (i) OPCS4 codes and (ii) Read codes that map to an OPCS4 code.

```{r}
# filter mapped self-reported op codes for selected CALIBER diseases only
ukb_self_report_cancer_to_caliber_map_selected <-
  ukb_self_report_cancer_to_caliber_map %>%
  filter(selected == "Y") %>%
  select(-selected)


# combine with CALIBER codes, which are filtered for ICD (i.e. not OPCS4) and
# Read codes that do not map to an OPCS4 code
list(
  ukb_self_report_cancer_to_caliber_map_selected,
  caliber_codes %>%
    # filter for ICD codes, and read codes that do *not* map to OPCS4
    filter((code_type %in% c("icd9", "icd10")) |
             ((code_type == "read2") &
                (!code %in% read_codes_that_map_to_opcs4$read2)
             ) |
             ((code_type == "read3") &
                (!code %in% read_codes_that_map_to_opcs4$read3)
             ))
) %>%
  bind_rows() %>%
  filter(disease %in% ukb_self_report_cancer_to_caliber_map_selected$disease) %>%
  arrange(disease,
          category,
          code_type) %>%
  reactable(
    filterable = TRUE,
    searchable = TRUE,
    paginationType = "jump",
    showPageSizeOptions = TRUE,
    resizable = TRUE,
    pageSizeOptions = c(10, 25, 50, 100, 200, 300, 350)
  )  
```

