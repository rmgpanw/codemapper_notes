---
title: "About"
output:
  workflowr::wflow_html:
    toc: false
    code_folding: hide
editor_options:
  chunk_output_type: console
---

```{r global-options, echo=FALSE}
# remove warning messages etc from final report
knitr::opts_chunk$set(
  warning = FALSE,
  message = FALSE
)
```

```{r results='hide'}
library(targets)
```

Overview of targets pipeline (will most likely not reflect actual pipeline status, but does show all targets):

```{r}
targets::tar_visnetwork(targets_only = TRUE)
```
