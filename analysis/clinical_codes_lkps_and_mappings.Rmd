---
title: "Clinical codes - lookups and mappings"
author: "Alasdair Warwick"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: 
  workflowr::wflow_html:
    toc: false
editor_options:
  chunk_output_type: console
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(codemapper)
library(tidyverse)
library(targets)

tar_load(ALL_LKPS_MAPS_DB)
```

# Introduction

This vignette describes how to work with clinical codes using `codemapper`. Specifically:

- Looking up codes and their descriptions
- Getting 'children' codes
- Mapping codes from one coding system to another

The functions provided by `codemapper` rely on [UK Biobank resource 592](https://biobank.ndph.ox.ac.uk/ukb/refer.cgi?id=592), which includes an Excel workbook containing lookup and mapping tables, and the NHSBSA BNF to SNOMED code mapping file (available [here](https://www.nhsbsa.nhs.uk/prescription-data/understanding-our-data/bnf-snomed-mapping)). These tables have been converted into a named list of data frames. This can be retrieved with `TODO()`:

```{r}
# retrieve code mappings in .Rdata format
targets::tar_load(all_lkps_maps)

# each item in the list is a sheet in the UKB Excel workbook (resource 592)
names(all_lkps_maps)
```

# Code lookups

To lookup details for a particular code, use `lookup_codes()`. Setting `preferred_description_only` to `TRUE` will return only the preferred code descriptions if synonyms are present (`read2` and `read3` may include multiple descriptions for the same code):

```{r}
# Some Read2 codes for T1DM
t1dm_read2 <- c("C10E.", "C108.")

# lookup details
lookup_codes(codes = t1dm_read2,
             code_type = "read2",
             all_lkps_maps = all_lkps_maps,
             preferred_description_only = TRUE)
```

By default, the output is standardised to produce the columns shown above. To output the original formatting from [UK Biobank resource 592](https://biobank.ndph.ox.ac.uk/ukb/refer.cgi?id=592), set `standardise_output` to `FALSE`:

```{r}
# lookup details
lookup_codes(codes = t1dm_read2,
             code_type = "read2",
             all_lkps_maps = all_lkps_maps,
             preferred_description_only = TRUE,
             standardise_output = FALSE)
```

# Code mapping

An example: map codes for type 1 diabetes from Read2 to Read3. Note that both of the Read 2 codes `C10E.` and `C108.` map to a single read3 code, `X40J4`:

```{r}
# Some Read2 codes for T1DM
t1dm_read2 <- c("C10E.", "C108.")

# lookup details
map_codes(codes = t1dm_read2, 
          from = "read2", 
          to = "read3", 
          all_lkps_maps = all_lkps_maps)
```

Note that `preferred_description_only` cannot be `TRUE` if `standardise_output` is `FALSE` with `map_codes` (will raise an error). This is because some codes may otherwise be 'lost' in the mapping process. When `standardise_output` is `TRUE`, the mapped codes from `map_codes` are passed on to `lookup_codes`, at which point one can request to return only preferred code descriptions:

```{r}
# mapping the Read 2 code "D4104" to Read 3 only returns the secondary Read 3
# description (`TERMV3_TYPE` == "S"), unlike for "D4103". 
map_codes(codes = c("D4103", "D4104"), 
          from = "read2", 
          to = "read3", 
          all_lkps_maps = all_lkps_maps, 
          codes_only = FALSE,
          preferred_description_only = FALSE,
          standardise_output = FALSE) %>% 
  dplyr::select(tidyselect::contains("V3"))
```

```{r}
# if `standardise_output` is `TRUE`, then `preferred_description_only` may also
# be set to `TRUE`
map_codes(codes = c("D4103", "D4104"), 
          from = "read2", 
          to = "read3", 
          all_lkps_maps = all_lkps_maps, 
          codes_only = FALSE,
          preferred_description_only = TRUE,
          standardise_output = TRUE)
```

Mapping to ICD is more problematic as some results are a range of ICD codes (note also that for ICD-10, the mapping sheets use an alternative code format which removes any "." characters):

```{r}
map_codes(codes = t1dm_read2, 
          from = "read2", 
          to = "icd10", 
          all_lkps_maps = all_lkps_maps, 
          codes_only = TRUE,
          standardise_output = FALSE, 
          # by default, an error is raised if any unrecognised codes are present
          unrecognised_codes = 'warning')
```

The available mappings do not cover all possible mapping directions. For example, while there are mappings for Read2 to ICD-10, there is no mapping for ICD-10 to Read2. For cases like this, by setting the `reverse_mapping` argument to 'warning' for `map_codes()` an attempt to map anyway by using the same mapping sheet in reverse (e.g. for mapping ICD-10 to Read2, `map_codes` uses the `read_v2_icd10` mapping sheet).

```{r}
# find ICD-10 code matching "diabetic retinopathy"
icd10_diabetic_retinopathy <-
  code_descriptions_like(
    reg_expr = "diabetic retinopathy",
    code_type = "icd10",
    all_lkps_maps = all_lkps_maps,
    ignore_case = TRUE,
    codes_only = TRUE
  )

# attempting to map this to Read 2 returns a NULL result however
map_codes(
  codes = icd10_diabetic_retinopathy,
  from = "icd10",
  to = "read2",
  all_lkps_maps = all_lkps_maps,
  standardise_output = TRUE,
  codes_only = FALSE, 
  reverse_mapping = 'warning'
) %>% 
  knitr::kable()
```

# Find codes that match a description

Use `code_descriptions_like()`. For example, to find Read2 codes that match the description 'diabetic retinopathy':

```{r}
code_descriptions_like(reg_expr = "diabetic retinopathy", 
                          code_type = "read2", 
                          all_lkps_maps = all_lkps_maps, 
                          ignore_case = TRUE, 
                          codes_only = FALSE,
                          preferred_description_only = TRUE) %>% 
  head() %>% 
  knitr::kable()
```

# Children codes

To get the children codes, use `codes_starting_with()`. This will return all unique clinical codes that start with the codes of interest:

```{r}
# Some Read2 codes for T1DM
t1dm_read2 <- c("C10E.", "C108.")

# lookup details
codes_starting_with(codes = t1dm_read2, 
                code_type = "read2", 
                all_lkps_maps = all_lkps_maps, 
                codes_only = TRUE,
                standardise_output = FALSE) %>% 
  knitr::kable()
```

> **Note:** Some coding systems include a '.' (e.g. ICD-10) - this may return unexpected results with `codes_starting_with()`, as this function searches using regexs and '.' is interpreted as a wildcard.

By default, a character vector of codes is returned. To return a data frame including code descriptions, set the argument `codes_only` to `FALSE`:

```{r}
# Some Read2 codes for T1DM
t1dm_read2 <- c("C10E.", "C108.")

# lookup details
codes_starting_with(codes = t1dm_read2, 
                code_type = "read2", 
                all_lkps_maps = all_lkps_maps, 
                codes_only = FALSE) %>% 
  head() %>% 
  knitr::kable()
```

# ICD-10

ICD-10 codes are presented in two different formats in the `icd10_lkp` table: `ICD_10` and `ALT_CODE`. The latter is how ICD-10 codes are recorded in UKB. However, an 'X' is appended to 3 character codes without any 4 character children (e.g. `A38X`, 'Scarlet fever'). 

Another issue with the `ICD_10` format is that is contains some duplicated codes e.g. `I70.0` appears 3 times. This is because of `MODIFIER_5` - the `ALT_CODE` format records a different code for each of these.
