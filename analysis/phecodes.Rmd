---
title: "Reformatting UKB 'all_lkps_maps.xlsx'"
author: "Alasdair Warwick"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: 
  workflowr::wflow_html:
    toc: false
editor_options:
  chunk_output_type: console
---

```{r global-options, echo=FALSE}
knitr::opts_chunk$set(warning=FALSE, message=FALSE, echo=FALSE)
```

```{r}
library(tidyverse)
library(reactable)
library(readxl)
library(crosstalk)
library(targets)
library(codemapper)
library(ggVennDiagram)

# phewas r package
# devtools::install_github("PheWAS/PheWAS")
library(PheWAS)

tar_load(ALL_LKPS_MAPS_DB)

# get UKB lookup and mapping tables
all_lkps_maps_raw <- tar_read(all_lkps_maps_raw)
all_lkps_maps <- tar_read(all_lkps_maps)
```

# Overview

**Aim:** to explore the phecode lookup and mapping files.

# Background - phecodes

- LINKS TO KEY PUBLICATIONS AND WEBPAGES
- WHAT CODING SYSTEM IS PHECODES BASED ON, AND WHICH CLINICAL CODES CAN BE MAPPED TO THIS?

# PheWAS R package

- READ HELP PAGE FROM RUNNING BELOW
- TO START, PERHAPS TRY RUNNING THE EXAMPLE (AT BOTTOM OF HELP PAGE) - COPY AND PASTE INTO THIS NOTEBOOK SO WE CAN SEE THE OUTPUT AT EACH STEP

```{r}
# Load the PheWAS package
# library(PheWAS)
#Set the random seed so it is replicable
set.seed(1)
```

# Generate some example data
```{r}
ex=generateExample()

# Extract the three parts from the returned list
id.vocab.code.count=ex$id.vocab.code.count
genotypes=ex$genotypes
id.sex=ex$id.sex
```

#Create the phecode table- translates the codes, adds exclusions, and reshapes to a wide format.
#Sum up the counts in the data where applicable.
```{r}
phenotypes = createPhenotypes(id.vocab.code.count,
                              aggregate.fun = sum,
                              id.sex = id.sex)
```

#Combine the data
```{r}
data=inner_join(inner_join(phenotypes,genotypes),id.sex)
```

#Run the PheWAS
```{r}
results = phewas_ext(
  data,
  phenotypes = names(phenotypes)[-1],
  genotypes = c("rsEXAMPLE"),
  covariates = c("sex"),
  cores = 1
)
```

#Plot the results
```{r}
phewasManhattan(results, 
  title="My Example PheWAS Manhattan Plot")
```

#Add PheWAS descriptions
```{r}
results_d=addPhecodeInfo(results)
```

#List the top 10 results
```{r}
results_d[order(results_d$p)[1:10],]
```

#Create a nice interactive table (eg, in RStudio)

```{r}
phewasDT(results)
```

# Mapping files

- DOWNLOAD PHECODE MAPPING FILES TO DATA DIRECTORY,THEN LOAD INTO R
- Downloaded files from ukbb pan ancestry https://github.com/atgu/ukbb_pan_ancestry 
```{r}
icd9_phecode_map <- read_csv(tar_read(PHECODE_1_2_ICD9_MAP))

icd10_phecode_map <- read_csv(tar_read(PHECODE_1_2_ICD10_MAP))

# remove "\\." from icd9_phecode_map as this is removed in all_lkps_maps$icd9_lkp
icd9_phecode_map <- icd9_phecode_map %>%
  mutate(icd9 = str_remove(string = icd9,
                           pattern = "\\."))
```

- How many ICD codes are included in the UK Biobank lookup table? (head shown below)

```{r}
# venn diagram of overlap
## icd10
list(
  icd10_lkp = unique(all_lkps_maps$icd10_lkp$ICD10_CODE),
  phecode_icd10 = unique(icd10_phecode_map$ICD10)
) %>% 
  ggVennDiagram()

##icd9
list(
  icd9_lkp = unique(all_lkps_maps$icd9_lkp$ICD9),
  phecode_icd9 = unique(icd9_phecode_map$icd9)
) %>% 
  ggVennDiagram()
```

- Which ICD-10 codes are not shared by phecode_map and all_lkps_maps
```{r}
# filter icd10
icd10_all_lkps_maps_not_in_phecode_map <- all_lkps_maps$icd10_lkp %>% filter(
  !all_lkps_maps$icd10_lkp$ICD10_CODE %in% icd10_phecode_map$ICD10
)

icd10_phecode_map_not_in_all_lkps_maps <- icd10_phecode_map %>% filter(
  !icd10_phecode_map$ICD10  %in% all_lkps_maps$icd10_lkp$ICD10_CODE
)

# filter icd9
icd9_all_lkps_maps_not_in_phecode_map <- all_lkps_maps$icd9_lkp %>% filter(
  !all_lkps_maps$icd9_lkp$ICD9 %in% icd9_phecode_map$icd9
)

icd9_phecode_map_not_in_all_lkps_maps <- icd9_phecode_map %>% filter(
  !icd9_phecode_map$icd9  %in% all_lkps_maps$icd9_lkp$ICD9
)
```

- How many ICD-10 codes map to >1 phecode? (I think this is mentioned on the phecode website, and in Spiros' github repo readme)
```{r}
icd10_phecode_map %>% count(ICD10) %>% filter(n>1) %>% nrow() 

icd10_phecode_map %>% count(ICD10) %>% filter(n>1) %>% reactable(filterable = TRUE,sortable = TRUE)
```


```{r}
icd10_phecode_map %>%
  filter(str_detect(
    string = PHECODE,
    pattern = "\\.",
    negate = TRUE
  )) %>%
  count(ICD10) %>% filter(n > 1) %>% nrow()

icd10_not_unique_phecodes <-icd10_phecode_map %>%
  filter(str_detect(
    string = PHECODE,
    pattern = "\\.",
    negate = TRUE
  )) %>%
  count(ICD10, sort = TRUE )%>%
filter(n>1)
```

- How many ICD-9 codes map to >1 phecode?  
```{r}
icd9_phecode_map %>% count(icd9) %>% filter(n>1) %>% nrow() 

icd9_phecode_map %>% count(icd9) %>% filter(n>1) %>% reactable(filterable = TRUE,searchable = TRUE)
```


```{r}
icd9_phecode_map %>%
  filter(str_detect(
    string = phecode,
    pattern = "\\.",
    negate = TRUE
  )) %>%
  count(icd9) %>% filter(n > 1) %>% nrow()

icd9_not_unique_phecodes <-icd9_phecode_map %>%
  filter(str_detect(
    string = phecode,
    pattern = "\\.",
    negate = TRUE
  )) %>%
  count(icd9, sort = TRUE )%>%
filter(n>1)
```
